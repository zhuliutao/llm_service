from langchain.embeddings.huggingface import HuggingFaceEmbeddings
from langchain.vectorstores import FAISS
from langchain.document_loaders import UnstructuredFileLoader
from config import *
import datetime
from knowledge_service.test_spliter import ChineseTextSplitter
from typing import List, Tuple
from langchain.docstore.document import Document
import numpy as np
from utils import torch_gc
from tqdm import tqdm
from pypinyin import lazy_pinyin
from knowledge_service.loader import UnstructuredPaddleImageLoader
from knowledge_service.loader import UnstructuredPaddlePDFLoader

DEVICE_ = EMBEDDING_DEVICE
DEVICE_ID = "0" if torch.cuda.is_available() else None
DEVICE = f"{DEVICE_}:{DEVICE_ID}" if DEVICE_ID else DEVICE_


def load_file(filepath, sentence_size=SENTENCE_SIZE):
    if filepath.lower().endswith(".md"):
        loader = UnstructuredFileLoader(filepath, mode="elements")
        docs = loader.load()
    elif filepath.lower().endswith(".pdf"):
        loader = UnstructuredPaddlePDFLoader(filepath)
        textsplitter = ChineseTextSplitter(pdf=True, sentence_size=sentence_size)
        docs = loader.load_and_split(textsplitter)
    elif filepath.lower().endswith(".jpg") or filepath.lower().endswith(".png"):
        loader = UnstructuredPaddleImageLoader(filepath, mode="elements")
        textsplitter = ChineseTextSplitter(pdf=False, sentence_size=sentence_size)
        docs = loader.load_and_split(text_splitter=textsplitter)
    else:
        loader = UnstructuredFileLoader(filepath, mode="elements")
        textsplitter = ChineseTextSplitter(pdf=False, sentence_size=sentence_size)
        docs = loader.load_and_split(text_splitter=textsplitter)
    write_check_file(filepath, docs)
    return docs


def write_check_file(filepath, docs):
    folder_path = os.path.join(os.path.dirname(filepath), "tmp_files")
    if not os.path.exists(folder_path):
        os.makedirs(folder_path)
    fp = os.path.join(folder_path, 'load_file.txt')
    fout = open(fp, 'a')
    fout.write("filepath=%s,len=%s" % (filepath, len(docs)))
    fout.write('\n')
    for i in docs:
        fout.write(str(i))
        fout.write('\n')
    fout.close()


def generate_prompt(related_docs: List[str], query: str,
                    prompt_template=PROMPT_TEMPLATE) -> str:
    context = "\n".join([doc[0].page_content for doc in related_docs])
    prompt = prompt_template.replace("{question}", query).replace("{context}", context)
    return prompt


def seperate_list(ls: List[int]) -> List[List[int]]:
    lists = []
    ls1 = [ls[0]]
    for i in range(1, len(ls)):
        if ls[i - 1] + 1 == ls[i]:
            ls1.append(ls[i])
        else:
            lists.append(ls1)
            ls1 = [ls[i]]
    lists.append(ls1)
    return lists




class LocalDocQA:
    llm: object = None
    embeddings: object = None
    top_k: int = VECTOR_SEARCH_TOP_K
    chunk_size: int = CHUNK_SIZE
    chunk_conent: bool = True
    score_threshold: int = VECTOR_SEARCH_SCORE_THRESHOLD

    @classmethod
    def init_model(cls):
        try:
            cls.init_cfg()
            cls.llm._call("你好")
            reply = """模型已成功加载，可以开始对话，或从右侧选择模式后开始对话"""
            logger.info(reply)
            return reply
        except Exception as e:
            logger.error(e)
            reply = """模型未成功加载，请到页面左上角"模型配置"选项卡中重新选择后点击"加载模型"按钮"""
            if str(e) == "Unknown platform: darwin":
                logger.info("报错了"
                            " ")
            else:
                logger.info(reply)
            return reply

    @classmethod
    def reinit_model(cls, llm_model, embedding_model, llm_history_len, use_ptuning_v2, use_lora, top_k, history):
        try:
            cls.init_cfg(llm_model=llm_model,
                                embedding_model=embedding_model,
                                llm_history_len=llm_history_len,
                                use_ptuning_v2=use_ptuning_v2,
                                use_lora=use_lora,
                                top_k=top_k, )
            model_status = """模型已成功重新加载，可以开始对话，或从右侧选择模式后开始对话"""
            logger.info(model_status)
        except Exception as e:
            logger.error(e)
            model_status = """模型未成功重新加载，请到页面左上角"模型配置"选项卡中重新选择后点击"加载模型"按钮"""
            logger.info(model_status)
        return history + [[None, model_status]]

    def init_cfg(self,
                 embedding_model: str = EMBEDDING_MODEL,
                 embedding_device=EMBEDDING_DEVICE,
                 llm_history_len: int = LLM_HISTORY_LEN,
                 llm_model: str = LLM_MODEL,
                 llm_device=LLM_DEVICE,
                 top_k=VECTOR_SEARCH_TOP_K,
                 use_ptuning_v2: bool = USE_PTUNING_V2,
                 use_lora: bool = USE_LORA,
                 ):
        if llm_model.startswith('QwenLLM'):
            from llm_service.qwenllm import QwenLLM
            self.llm = QwenLLM()
        else:
            from llm_service.chatgml import ChatGLM2
            self.llm = ChatGLM2()
        # self.llm.load_model(model_name_or_path=llm_model_dict[llm_model],
        #                     llm_device=llm_device, use_ptuning_v2=use_ptuning_v2, use_lora=use_lora)
        self.llm.history_len = llm_history_len

        self.embeddings = HuggingFaceEmbeddings(model_name=embedding_model_dict[embedding_model],
                                                model_kwargs={'device': embedding_device},)
        self.top_k = top_k

    def init_knowledge_vector_store(self,
                                    filepath: str or List[str],
                                    vs_path: str or os.PathLike = None,
                                    sentence_size=SENTENCE_SIZE):
        loaded_files = []
        failed_files = []
        if isinstance(filepath, str):
            if not os.path.exists(filepath):
                print("路径不存在")
                return None
            elif os.path.isfile(filepath):
                file = os.path.split(filepath)[-1]
                try:
                    docs = load_file(filepath, sentence_size)
                    logger.info(f"{file} 已成功加载")
                    loaded_files.append(filepath)
                except Exception as e:
                    logger.error(e)
                    logger.info(f"{file} 未能成功加载")
                    return None
            elif os.path.isdir(filepath):
                docs = []
                for file in tqdm(os.listdir(filepath), desc="加载文件"):
                    fullfilepath = os.path.join(filepath, file)
                    try:
                        docs += load_file(fullfilepath, sentence_size)
                        loaded_files.append(fullfilepath)
                    except Exception as e:
                        logger.error(e)
                        failed_files.append(file)

                if len(failed_files) > 0:
                    logger.info("以下文件未能成功加载：")
                    for file in failed_files:
                        logger.info(f"{file}\n")

        else:
            docs = []
            for file in filepath:
                try:
                    docs += load_file(file)
                    logger.info(f"{file} 已成功加载")
                    loaded_files.append(file)
                except Exception as e:
                    logger.error(e)
                    logger.info(f"{file} 未能成功加载")
        if len(docs) > 0:
            logger.info("文件加载完毕，正在生成向量库")
            if vs_path and os.path.isdir(vs_path):
                vector_store = FAISS.load_local(vs_path, self.embeddings)
                vector_store.add_documents(docs)
                torch_gc()
            else:
                if not vs_path:
                    vs_path = os.path.join(VS_ROOT_PATH,
                                           f"""{"".join(lazy_pinyin(os.path.splitext(file)[0]))}_FAISS_{datetime.datetime.now().strftime("%Y%m%d_%H%M%S")}""")
                vector_store = FAISS.from_documents(docs, self.embeddings)  # docs 为Document列表
                torch_gc()

            vector_store.save_local(vs_path)
            return vs_path, loaded_files
        else:
            logger.info("文件均未成功加载，请检查依赖包或替换为其他文件再次上传。")
            return None, loaded_files

    def one_knowledge_add(self, vs_path, one_title, one_conent, one_content_segmentation, sentence_size):
        try:
            if not vs_path or not one_title or not one_conent:
                logger.info("知识库添加错误，请确认知识库名字、标题、内容是否正确！")
                return None, [one_title]
            docs = [Document(page_content=one_conent + "\n", metadata={"source": one_title})]
            if not one_content_segmentation:
                text_splitter = ChineseTextSplitter(pdf=False, sentence_size=sentence_size)
                docs = text_splitter.split_documents(docs)
            if os.path.isdir(vs_path):
                vector_store = FAISS.load_local(vs_path, self.embeddings)
                vector_store.add_documents(docs)
            else:
                vector_store = FAISS.from_documents(docs, self.embeddings)  ##docs 为Document列表
            torch_gc()
            vector_store.save_local(vs_path)
            return vs_path, [one_title]
        except Exception as e:
            logger.error(e)
            return None, [one_title]

    def get_knowledge_based_answer(self, query, vs_path, chat_history=[], streaming: bool = STREAMING):
        vector_store = FAISS.load_local(vs_path, self.embeddings, allow_dangerous_deserialization=True)
        # FAISS.similarity_search_with_score_by_vector = similarity_search_with_score_by_vector
        vector_store.chunk_size = self.chunk_size
        vector_store.chunk_conent = self.chunk_conent
        vector_store.score_threshold = self.score_threshold
        related_docs_with_score = vector_store.similarity_search_with_score(query, k=self.top_k)
        torch_gc()
        prompt = generate_prompt(related_docs_with_score, query)

        for result, history in self.llm._call(prompt=prompt,
                                              history=chat_history,
                                              streaming=streaming):
            torch_gc()
            history[-1][0] = query
            response = {"query": query,
                        "result": result,
                        "source_documents": related_docs_with_score}
            yield response, history
            torch_gc()

    # query      查询内容
    # vs_path    知识库路径
    # chunk_conent   是否启用上下文关联
    # score_threshold    搜索匹配score阈值
    # vector_search_top_k   搜索知识库内容条数，默认搜索5条结果
    # chunk_sizes    匹配单段内容的连接上下文长度
    def get_knowledge_based_conent_test(self, query, vs_path, chunk_conent,
                                        score_threshold=VECTOR_SEARCH_SCORE_THRESHOLD,
                                        vector_search_top_k=VECTOR_SEARCH_TOP_K, chunk_size=CHUNK_SIZE):
        vector_store = FAISS.load_local(vs_path, self.embeddings, allow_dangerous_deserialization=True)

        vector_store.chunk_conent = chunk_conent
        vector_store.score_threshold = score_threshold
        vector_store.chunk_size = chunk_size
        related_docs_with_score = vector_store.similarity_search_with_score(query, k=vector_search_top_k)
        if not related_docs_with_score:
            response = {"query": query,
                        "source_documents": []}
            return response, ""
        torch_gc()
        prompt = generate_prompt(related_docs_with_score, query)
        response = {"query": query,
                    "source_documents": related_docs_with_score}
        return response, prompt


if __name__ == "__main__":
    # local_doc_qa = LocalDocQA()
    # local_doc_qa.init_cfg()
    # query = "windows 10 怎样安装"
    # vs_path = "/Users/jackson/Documents/Fintech/project/own_rag/vector_store/test2"
    # last_print_len = 0
    # resp = ""
    # data = local_doc_qa.get_knowledge_based_answer(query=query,
    #                                                              vs_path=vs_path,
    #                                                              chat_history=[],
    #                                                              streaming=True)
    # print(data)
    # for resp, history in local_doc_qa.get_knowledge_based_answer(query=query,
    #                                                              vs_path=vs_path,
    #                                                              chat_history=[],
    #                                                              streaming=True):
    #     logger.info(resp["result"][last_print_len:], end="", flush=True)
    #     print(resp)
    #     last_print_len = len(resp["result"])
    # source_text = [f"""出处 [{inum + 1}] {os.path.split(doc.metadata['source'])[-1]}：\n\n{doc.page_content}\n\n"""
    #                # f"""相关度：{doc.metadata['score']}\n\n"""
    #                for inum, doc in
    #                enumerate(resp["source_documents"])]
    # logger.info("\n\n" + "\n\n".join(source_text))
    # Embedding model name
    # EMBEDDING_MODEL = "text2vec"
    #
    # # Embedding running device
    # EMBEDDING_DEVICE = "cuda" if torch.cuda.is_available() else "mps" if torch.backends.mps.is_available() else "cpu"
    # embeddings = HuggingFaceEmbeddings(model_name=embedding_model_dict[EMBEDDING_MODEL],
    #                                    model_kwargs={'device': EMBEDDING_DEVICE})

    # def test_init_cfg():
    #     # 创建 LocalDocQA 实例
    #     local_doc_qa = LocalDocQA()
    #
    #     # 调用 init_cfg 方法进行初始化
    #     local_doc_qa.init_cfg(
    #     )
    #
    #     # 检查初始化是否成功
    #     assert local_doc_qa.llm is not None, "LLM initialization failed"
    #     assert local_doc_qa.embeddings is not None, "Embeddings initialization failed"
    #
    #     print("Initialization test passed")
    #
    #
    # # 运行测试
    # test_init_cfg()

    def test_local_doc_qa():
        # 创建 LocalDocQA 实例
        local_doc_qa = LocalDocQA()

        # 初始化配置
        local_doc_qa.init_cfg(

        )
        logger.info("配置初始化成功")

        # 定义要加载的文件路径
        filepath = "/Users/jackson/Documents/Fintech/project/own_rag/content/test2/安装Windows 10系统通常包括以下几个步骤.pdf"  # 替换为实际的文件路径或目录

        # 定义向量存储路径
        vs_path = "/Users/jackson/Documents/Fintech/project/own_rag/vector_store/test2"  # 替换为实际的向量存储路径

        # 调用 init_knowledge_vector_store 方法
        vs_path, loaded_files = local_doc_qa.init_knowledge_vector_store(
            filepath=filepath,
            vs_path=vs_path,
            sentence_size=100
        )

        # 检查向量存储是否成功初始化
        print(vs_path)
        print(len(loaded_files) > 0)
        assert vs_path is not None, "向量存储初始化失败"
        assert len(loaded_files) > 0, "没有成功加载的文件"
        logger.info(f"向量存储初始化成功，存储路径: {vs_path}")
        logger.info(f"成功加载的文件: {loaded_files}")


    test_local_doc_qa()
