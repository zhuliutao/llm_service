import os
import time
import config
from langchain.document_loaders import PyPDFLoader, CSVLoader, TextLoader
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.vectorstores.faiss import FAISS
from db_service.sqlit_service import select_active_db
from langchain.embeddings import HuggingFaceEmbeddings
from config import *


KNOWLEDGE_FILE_PATH = '../data/db'
EMBEDDING_MODEL_PATH = ''

active_db = {}


def create_base(file_path: str, kb_name) -> str:
    """
    创建PDF文件向量库
    :param kb_name:
    :param file_path: 文件路径
    :return:
    """
    loader = None
    try:
        if file_path.endswith('pdf'):
            loader = PyPDFLoader(file_path, encoding='utf-8')
        elif file_path.endswith('csv'):
            loader = CSVLoader(file_path, encoding='utf-8')
        else:
            loader = TextLoader(file_path, encoding='utf-8')

        print(f'处理的文件： {file_path}')
        print("创建向量数据库中... %s", time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
        docs = loader.load()
        # 文本分割
        text_splitter = RecursiveCharacterTextSplitter(chunk_size=500, chunk_overlap=200)
        docs = text_splitter.split_documents(docs)

        # 向量化
        embedding = HuggingFaceEmbeddings(model_name=embedding_model_dict["moka-ai/m3e-small"])  #应为"m3e-small"
        # 构造向量库+conversation_id
        save_path = os.path.join(config.DB_FILE_PATH, kb_name)

        # 创建向量数据库
        vectordb = FAISS.from_documents(documents=docs, embedding=embedding)
        print("向量数量：", vectordb.index.ntotal)
        vectordb.save_local(save_path)
        print("向量数据库创建完！ %s", time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))
        return "success"
    except Exception as e:
        raise e


def load_db(db_path: str):
    if not os.path.exists(db_path):
        print(db_path)
        raise Exception('不存在该数据库！')
    else:
        embedding = HuggingFaceEmbeddings(model_name=embedding_model_dict["moka-ai/m3e-small"])
        db = FAISS.load_local(db_path, embedding, allow_dangerous_deserialization=True)
        print('-------------数据库加载成功-------------')
        return db


def init_db_service():
    try:
        alias = select_active_db()
        if alias != '-1':
            create_db_service(alias)
        else:
            raise Exception('未查询到活跃数据库')
    except Exception as e:
        raise e


def create_db_service(db_name):
    db_path = os.path.join(config.DB_FILE_PATH, db_name)
    active_db[db_name] = load_db(db_path)

# file_path = '../data/installwindow10.txt'
#
# create_base(file_path,'csdb')

# db_path = '../data/db/csdb'
# embedding = ChatGLMEmbeddings()
# new_db = FAISS.load_local(db_path, embedding, allow_dangerous_deserialization=True)
# docs = new_db.similarity_search('准备安装介质', k=2)
# docs[0]
# load_db(db_path)

# init_db_service()
