from typing import List
from langchain_community.document_loaders.unstructured import UnstructuredFileLoader
from paddleocr import PaddleOCR
import os
import fitz

class UnstructuredPaddlePDFLoader(UnstructuredFileLoader):
    """Loader that uses unstructured to load image files, such as PNGs and JPGs."""

    def _get_elements(self) -> List:
        def pdf_ocr_txt(filepath, dir_path="tmp_files"):
            full_dir_path = os.path.join(os.path.dirname(filepath), dir_path)
            if not os.path.exists(full_dir_path):
                os.makedirs(full_dir_path)
            filename = os.path.split(filepath)[-1]
            ocr = PaddleOCR(lang="ch", use_gpu=False, show_log=False)
            doc = fitz.open(filepath)
            txt_file_path = os.path.join(full_dir_path, f"{filename}.txt")
            img_name = os.path.join(full_dir_path, '.tmp.png')

            with open(txt_file_path, 'w', encoding='utf-8') as fout:
                for i in range(doc.page_count):
                    page = doc[i]
                    text = page.get_text("")
                    fout.write(text)
                    fout.write("\n")

                    img_list = page.get_images()
                    for img_index, img in enumerate(img_list):
                        pix = fitz.Pixmap(doc, img[0])
                        pix.save(img_name)
                        result = ocr.ocr(img_name)

                        if result and result[0]:
                            ocr_result = [line[1][0] for line in result[0] if line[1][0] is not None]
                            fout.write("\n".join(ocr_result))
                            fout.write("\n")
                        else:
                            print(f"No OCR result for image {img_index + 1} on page {i + 1}")

                        # if os.path.exists(img_name):
                        #     os.remove(img_name)

            return txt_file_path

        txt_file_path = pdf_ocr_txt(self.file_path)
        from unstructured.partition.text import partition_text
        return partition_text(filename=txt_file_path, **self.unstructured_kwargs)

if __name__ == "__main__":
    filepath = os.path.join(os.path.dirname(os.path.dirname(__file__)), "data/test_files/安装Windows 10系统通常包括以下几个步骤.pdf")
    loader = UnstructuredPaddlePDFLoader(filepath, mode="elements")
    docs = loader.load()
    print(docs)
    for doc in docs:
        print(doc)

    # 初始化文本分割器

