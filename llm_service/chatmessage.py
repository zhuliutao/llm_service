from langchain.embeddings import HuggingFaceEmbeddings
from langchain.prompts import ChatPromptTemplate
from langchain.prompts.example_selector import SemanticSimilarityExampleSelector
from langchain.vectorstores import Chroma
from langchain.prompts import ChatPromptTemplate, FewShotChatMessagePromptTemplate
import os

import config
from api_service import configs

from llm_service.qwenllm import QwenLLM
from llm_service.chatgml import ChatGLM2


# 获取当前脚本文件的目录
# current_dir = os.path.dirname(__file__)


# 构建相对路径,知识云QA.txt里是网络类故障处理方法的QA对
# file_path = os.path.join(current_dir, '../data/知识云QA.txt')
# model_path = os.path.join(current_dir, '../model/embeddings/m3e-small')

class GetFewShotChatMessage():
    def get_exam(self, file_path):
        examples = []
        with open(file_path, "r", encoding='utf8') as f:  # 打开文件
            content = f.readlines()
            result = {}
            for i in content:
                if "问题" in i:
                    result['question'] = i.split('：')[1]
                if "答案" in i:
                    result['answer'] = ''.join(i.split('：')[1:])
                    examples.append(result)
                    result = {}
        return examples

    def get_FSCMPromptTemplate(self, model_path, file_path):
        examples = self.get_exam(file_path)
        to_vectorize = [" ".join(example.values()) for example in examples]
        embeddings = HuggingFaceEmbeddings(model_name=model_path)
        vectorstore = Chroma.from_texts(to_vectorize, embeddings, metadatas=examples)

        example_selector = SemanticSimilarityExampleSelector.from_examples(
            examples,
            # 用于生成嵌入的嵌入类，用于测量语义相似性。
            embeddings,
            # 用于存储嵌入并进行相似性搜索的VectorStore类。
            vectorstore,
            # 要生成的示例数量。
            k=3
        )
        # # The prompt template will load examples by passing the input do the `select_examples` method
        # example_selector.select_examples({"input": "网络故障"})
        example_prompt = ChatPromptTemplate.from_messages(
            [('human', '{question}'), ('ai', '{answer}')]
        )

        few_shot_prompt = FewShotChatMessagePromptTemplate(
            # input variables选择要传递给示例选择器的值
            input_variables=["question"],
            example_selector=example_selector,
            # 定义每个示例的格式。在这种情况下，每个示例将变成 2 条消息：
            # 1 条来自人类，1 条来自 AI
            example_prompt=example_prompt
        )

        final_prompt = ChatPromptTemplate.from_messages(
            [
                ('system', 'You are a helpful AI Assistant'),
                few_shot_prompt,
                ('human', '{question}'),
            ]

        )
        # print(few_shot_prompt.format(question="不能上网怎么办"))
        return final_prompt


if __name__ == '__main__':
    from langchain_core.output_parsers import StrOutputParser


    # llm = ChatGLM2()
    ls = GetFewShotChatMessage()
    final_prompt = ls.get_FSCMPromptTemplate(model_path=configs.KNOWLEDGE_MODEL_PATH,
                                             file_path=configs.KNOWLEDGE_FILE_PATH)
    # chain = final_prompt | llm
    question = "不能上网怎么办"
    ls = GetFewShotChatMessage()
    prompt = final_prompt.format_prompt(question = question).to_string()
    print(prompt)
