from abc import ABC

from langchain.llms.base import LLM
from langchain_core.callbacks import CallbackManagerForLLMRun
from typing import Optional, List, Any
from http import HTTPStatus
import dashscope
from dashscope import Generation
# from llm_service.qwen_key_config import QWEN_OPENAI_BASE_URL, QWEN_API_KEY

QWEN_API_KEY = 	"sk-e253ad28471242e79c7a65e5107a6404"
class QwenLLM(LLM):
    model: str = "qwen-turbo"
    # model: str = "qwen-plus"
    max_token: int = 2048
    temperature: float = 0.1
    top_p = 0.7
    history = []
    history_len: int = 20

    def __init__(
            self,
            api_key=QWEN_API_KEY,
            model=model,
            temperature=temperature,
            top_p=top_p,
            history=history):
        super().__init__()
        dashscope.api_key = api_key
        self.temperature = temperature
        self.top_p = top_p
        self.history = history
        self.model = model

    @property
    def _llm_type(self) -> str:
        return self.model
    
    def history2message(self, history: List[List[str]]):
        message = []
        for h in history:
            message += [
                {'role': 'user','content': h[0]},
                {'role': 'assistant', 'content': h[1]}
                ]
        print(message)
        return message

    def _call(
            self,
            prompt: str,
            history=[],
            stop: Optional[List[str]] = None,
            run_manager: Optional[CallbackManagerForLLMRun] = None,
            **kwargs: Any):
        print("--------------------1、prompt-----------------------")
        print("QwenLLM prompt:", prompt)
        print("--------------------2、大模型开始-----------------------")
        response = Generation.call(
            model=self.model,
            prompt=prompt,
            messages=self.history2message(history)
        )

        if response.status_code != HTTPStatus.OK:
            return "查询结果错误"
        resp = response.output

        self.history = self.history + [[prompt, resp['text']]]
        print("模型输出：", resp['text'].replace(';', ''))
        return resp['text'].replace(';', '')

# if __name__ == "__main__":
#
#     llm = QwenLLM()
#     response, _ = llm._call("早上好", history=[["hello", "hi"]])
#     print(response)