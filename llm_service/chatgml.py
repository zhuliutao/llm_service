import json
import requests

from langchain.llms.base import LLM
from langchain_community.llms.utils import enforce_stop_tokens
from langchain_core.callbacks import CallbackManagerForLLMRun
from typing import List, Optional

base_url = "http://101.42.24.69:8000"

class ChatGLM2(LLM):
    max_token: int = 2048
    temperature: float = 0.1
    top_p = 0.7
    history = []

    def __init__(self, temperature=temperature, top_p=top_p, history=history):
        super().__init__()
        self.temperature = temperature
        self.top_p = top_p
        self.history = history

    @property
    def _llm_type(self) -> str:
        return "ChatGLM2"

    def _call(self, 
              prompt: str, 
              stop: Optional[List[str]] = None,
              history=[],
              run_manager: Optional[CallbackManagerForLLMRun] = None, **kwargs) -> str:
        headers = {'Content-Type': 'application/json'}
        print(prompt, history)
        data = json.dumps({
            'prompt': prompt,
            'temperature': self.temperature,
            'history': history,
            'max_length': self.max_token})
        print("ChatGLM prompt:", prompt)
        print("***--------------------------------***-----------------------------------------***")
        # 调用api
        response = requests.post(base_url, headers=headers, data=data, timeout=500)
        if response.status_code != 200:
            return "查询结果错误"
        resp = response.json()
        text_response = resp['response']
        if stop is not None:
            response = enforce_stop_tokens(text_response, stop)
        self.history = history + [[prompt, resp['response']]]
        print('输出：', resp['response'])

        return resp['response'].replace(';', '')

# llm = ChatGLM2()
# resp = llm._call('您好')
# print(resp)
