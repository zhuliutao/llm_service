
import json
import requests
from langchain.llms.base import LLM
from langchain.llms.utils import enforce_stop_tokens
from langchain_core.callbacks import CallbackManagerForLLMRun
# from langchain_core.runnables.config import run_in_executor
from typing import List, Optional

"""
使用MoE架构的大模型，问题出在它没有直接可以调的status_code，官方客服说返回的chatcompletions对象本身不含状态码，需要按openai.APIError 对象获取非200的状态信息，待修改，下面代码通
"""



api_key = "sk-5bb0a4322ca449c2979612d440bf0e6d"
base_url= r"https://api.deepseek.com"

class DeepSeek(LLM):
    model: str = "deepseek-chat"
    max_token: int = 2048
    temperature: float = 1.0
    top_p = 0.7
    history = []

    def __init__(self, temperature=temperature, top_p=top_p, history=history):
        super().__init__()
        self.temperature = temperature
        self.top_p = top_p
        self.history = history

    @property
    def _llm_type(self) -> str:
        return self.model

    def _call(self, prompt: str, stop: Optional[List[str]] = None, run_manager: Optional[CallbackManagerForLLMRun] = None, **kwargs) -> str:
        headers = {'Content-Type': 'application/json'}
        data = json.dumps({
            'prompt': prompt,
            'temperature': self.temperature,
            'history': self.history,
            'max_length': self.max_token})
        print(self.model," prompt:", prompt)
        from openai import OpenAI
        client = OpenAI(api_key=api_key, base_url=base_url)
        response = client.chat.completions.create(
            model=self.model,
            messages=[
                {"role": "system", "content": "You are a helpful assistant"},
                {"role": "user", "content": "Hello"},
            ],
            stream=False
        )
        if stop is not None:
            response = enforce_stop_tokens(response.choices[0].message.content, stop)
        self.history = self.history + [[None, response.choices[0].message.content]]
        return response.choices[0].message.content.replace(';', '')

        # if response.status_code != 200:
        #     return "查询结果错误"






if __name__ == "__main__":

    llm = DeepSeek()
    response = llm.invoke("你是谁？")
    print(response)

