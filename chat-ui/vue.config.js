const { defineConfig } = require('@vue/cli-service')
const path = require("path");
function resolve(dir) {
  return path.join(__dirname, dir)
}
module.exports = defineConfig({
  transpileDependencies: true,
  configureWebpack: {
    name: 'chat-ui',
    resolve: {
      alias: {
        '@': resolve('src')
      }
    }
  },
  devServer: {
    proxy: {
      // detail: https://cli.vuejs.org/config/#devserver-proxy
      "/dev-api": {
        target: `http://127.0.0.1:3000`,
        changeOrigin: true,
        pathRewrite: {
          'dev-api': ''
        }
      }
    },
    // disableHostCheck: true
  }
})


