import request from "@/utils/request";

export function getInfo() {
    return request({
        url: '/info',
        method: 'get',
    })
}

export function chat(data) {
    return request({
        url: '/chat',
        data: data,
        method: 'post',
    })
}

export function hot(data) {
    return request({
        url: '/hot',
        data: data,
        method: 'post',
    })
}

export function uploadFile(data) {
    return request({
        url: '/upload',
        data: data,
        method: 'post',
    })
}
