import request from "@/utils/request";

export function getDbList() {
    return request({
        url: '/list_db',
        method: 'get',
    })
}

export function getEmb() {
    return request({
        url: '/get_embedding_models',
        method: 'get',
    })
}

export function getLLM() {
    return request({
        url: '/get_llm_models',
        method: 'get',
    })
}

export function chat(data) {
    return request({
        url: '/local_doc_chat',
        data: data,
        method: 'post',
    })
}

export function addDb(data) {
    return request({
        url: '/add_db',
        data: data,
        method: 'post',
    })
}

export function hot(data) {
    return request({
        url: '/hot',
        data: data,
        method: 'post',
    })
}

export function initLLm(data) {
    return request({
        url: '/init',
        data: data,
        method: 'post',
    })
}

export function uploadFile(data) {
    return request({
        url: '/upload_files',
        data: data,
        method: 'post',
    })
}

export function getFscmPrompTemplate(data) {
    return request({
        url: '/get_fscm_promp_template',
        data: data,
        method: 'post',
    })
}

export function uploadFile2(data) {
    let formData = new FormData()
    for (let file of data.files) {
        formData.append('files', file)
    }
    formData.append('knowledge_base_id', data.knowledge_base_id)
    return request({
        url: '/upload_files',
        data: formData,
        method: 'post',
        // headers: { 'Content-Type': 'multipart/form-data'}
    })
}

// 删除我的上报
export function deleteDb(dbName) {
    return request({
        url: '/delete_db/' + dbName,
        method: 'delete'
    })
}
