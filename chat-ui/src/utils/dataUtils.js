export function getNowTime(){
    // 获取当前北京时间的时间戳（单位为毫秒）
    let stamp= new Date().getTime() + 8 * 60 * 60 * 1000;
    // 格式化北京时间为"YYYY-MM-DD HH:mm:ss"
    let beijingTime = new Date(stamp).toISOString().replace(/T/, ' ').replace(/\..+/, '').substring(0, 19);
    return beijingTime
}



export function sleep(d){
    let t = Date.now()
    while(Date.now() - t <= d);
}

