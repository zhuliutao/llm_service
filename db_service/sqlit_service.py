import sqlite3
import config
import time
from typing import List, Tuple

# 连接到数据库 (如果数据库不存在则创建)
conn = sqlite3.connect(config.SQLIT_DATA_PATH, check_same_thread=False)


def create_table():
    # 创建一个 cursor 对象
    cur = conn.cursor()
    # 执行创建表的 SQL 语句
    cur.execute("""
    CREATE TABLE IF NOT EXISTS vector_db (
        id INTEGER PRIMARY KEY,
        name varchar(256) NOT NULL,
        alias varchar(64) NOT NULL,
        create_time varchar(32) NOT NULL
    )""")

    cur.execute("""
    CREATE TABLE IF NOT EXISTS chat_record (
        id INTEGER PRIMARY KEY,
        chat TEXT NOT NULL,
        mine BOOLEAN NOT NULL,
        name varchar(128) NOT NULL,
        create_time varchar(32) NOT NULL
    )""")

    cur.execute("""
    CREATE TABLE IF NOT EXISTS hot_problem (
        id INTEGER PRIMARY KEY,
        question TEXT NOT NULL,
        answer TEXT NOT NULL,
        create_time varchar(32) NOT NULL
    )""")
    # 提交事务
    conn.commit()
    cur.close()


def insert_vector_db_data(name, alias, file):
    # 创建一个cursor对象
    cur = conn.cursor()
    try:
        insert = "INSERT INTO vector_db(name,alias,file,create_time) VALUES (?,?,?)"
        create_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
        # 插入数据
        cur.execute(insert, (name, alias, file, create_time))
        # 提交事务
        conn.commit()
    except Exception as e:
        print(str(e))
        conn.rollback()
    finally:
        cur.close()
        conn.close()


def insert_chat_record_data(chats: List[Tuple]):
    # 创建一个cursor对象
    cur = conn.cursor()
    try:
        insert = "INSERT INTO chat_record(chat,mine,name,create_time) VALUES (?,?,?,?)"
        # 插入数据
        cur.executemany(insert, chats)
        # 提交事务
        conn.commit()
    except Exception as e:
        print(str(e))
        conn.rollback()
    finally:
        cur.close()


def select_vector_db_data():
    # 创建一个cursor对象
    cur = conn.cursor()
    try:
        sql = "select * from vector_db"
        # 查询数据
        return cur.execute(sql).fetchall()
    except Exception as e:
        raise e
    finally:
        cur.close()


def select_chat_record_data(k: int):
    # 创建一个cursor对象
    cur = conn.cursor()
    try:
        sql = f"select * from chat_record limit {k}"
        # 查询数据
        return cur.execute(sql).fetchall()
    except Exception as e:
        raise e
    finally:
        cur.close()


def select_hot_problem_data():
    # 创建一个cursor对象
    cur = conn.cursor()
    try:
        sql = "select * from hot_problem where type = '1'"
        # 查询数据
        return cur.execute(sql).fetchall()
    except Exception as e:
        raise e
    finally:
        cur.close()


def select_active_db() -> str:
    cur = conn.cursor()
    try:
        sql = "select alias from vector_db where active = '1'"
        db_data = cur.execute(sql).fetchall()
        if len(db_data) != 0:
            return db_data[0][0]
        else:
            return '-1'
    except Exception as e:
        raise e

# resp = select_chat_record_data(2)
# print(resp)

# create_table()
