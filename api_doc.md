# /list_db
- GET
- return 
    {
    "code": 200,
    "msg": "success",
    "data": [
        "db1"
        ]
    }

# /local_doc_chat
- POST
- input
    {
        "knowledge_base_id": "KB1",
        "question": "hello",
        "history": [],
        "time_stamp": "",
    }
  - return
      {
          "query": "hello",
          "response": "你好！有什么我可以帮助你的吗？",
          "history": [
              [
              "hello",
              "你好！有什么我可以帮助你的吗？"
              ]
          ],
          "source_documents": [
              "出处 [1] 安装Windows 10系统通常包括以下几个步骤.pdf：\n\n  请确保你在开始安装之前备份所有重要文件，  并确保你的电脑满足Windows 10 的最低系统  要求。\n\n",
              "出处 [2] 安装Windows 10系统通常包括以下几个步骤.pdf：\n\n安装Windows 10 系统通常包括以下几个步骤：  准备安装介质：  首先，  你需要一个Windows 10 的安装介质。\n\n"
          ],
          "robot_time": "2024-05-29 16:17:21",
      }

# /upload_file
- POST
    {
        "knowledge_base_id": "KB1",
        "file_name":"text.txt",
        "file_base64": "55So5oi35Y+N5pig5Y+356CBMTk5Njk5NTkyMDTkv6Hlj7flvojlt67vvIzlnKjlronlvr3oipzmuZbluILpuKDmsZ/ljLrljYPlspvmuZbot6/lrpzlsYXok53psrjmub7lsI/ljLrvvIzor7fogZTns7vlpITnkIbvvIzosKLosKIgIDEzMzMzNjE5NDY2XG7op6PlhrPmlrnmoYg6IOaKleivieWkhOeQhu+8muW3suWPkeefreS/oeWRiuefpSMmJeW3suWkhOeQhl/lhbblroMnDQrnlKjmiLflnKjlronlvr3nnIHml6DkuLrluILliJjmuKHplYflrZnmub7ooYzmlL/mnZHnjovmnZHmiYvmnLrkv6Hlj7fkuI3lpb3vvIzng6bor7fogZTns7vlpITnkIbjgJDoipzmuZbml6Dnur/jgJEgIDE5MTA5ODQ2MTM4XG7op6PlhrPmlrnmoYg6IOmXrumimOW9kuWxnu+8mjRH5pWw5o2uXG7mipXor4nliKTmlq3vvJrlpJrmrKHogZTns7vnlKjmiLfvvIzlnYfml6DkurrmjqXlkKzvvJtcbuaKleivieWkhOeQhu+8muW3suWPkeefreS/oeWRiuefpSMmJeW3suWkhOeQhl/lhbblroMnIA0K5oqV6K+J5L+h5oGvOiDnlKjmiLflj43mmKDlnKjlronlvr3nnIHoipzmuZbluILml6DkuLrluILopYTlronplYfkuIrnvZHmhaLvvIzmiYvmnLrkv6Hlj7fkuI3lpb3vvIzng6bor7fml6Dnur/ogZTns7vlpITnkIbvvIzosKLosKLjgJDoipzmuZbml6Dnur8744CRIDE3NzQzNTY5MTc1XG7op6PlhrPmlrnmoYg6IOmXrumimOW9kuWxnu+8mjRH5pWw5o2uXG7mipXor4nliKTmlq3vvJrogZTns7vnlKjmiLfooajnpLrnm67liY3kv6Hlj7flt7LmgaLlpI3mraPluLjvvJtcbuaKleivieWkhOeQhu+8muW7uuiurueUqOaIt+e7p+e7reinguWvn+S9v+eUqO+8jOeUqOaIt+iupOWPryMmJeW3suino+WGs1/lhbblroMn", 
    }
- return
    {
        "code": 200,
        "msg": "success"
    }

# /add_db
- POST    
    {
        "knowledge_base_id": "KB1"
    }
- return
    {
        "code": 200,
        "msg": "success"
    }

# /init
- POST
    {
        "llm_model": "qwen",
        "embedding_model": "text2vec",
        "llm_history_len": "3",
        "top_k": "2",
    }
- return
    {
        "code": 200,
        "msg": "success"
    }