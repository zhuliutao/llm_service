import os
import time
import argparse
from typing import List, Optional
from typing_extensions import Annotated

import uvicorn

import shutil
import uuid
import base64
# import config
from config import *
from flask import Flask
from flask import request
from knowledge_service.deal_knowledge import create_base, load_db, init_db_service, active_db
from db_service.sqlit_service import insert_vector_db_data, insert_chat_record_data, select_chat_record_data, \
    select_vector_db_data, select_hot_problem_data
from llm_service.qwenllm import QwenLLM
from llm_service.chatgml import ChatGLM2

import pydantic
from pydantic import BaseModel
from fastapi import Body, FastAPI, File, Form, Query, UploadFile, WebSocket

llm = ChatGLM2()
# llm = QwenLLM()

app = FastAPI()

db_dict = {}

knowledge_base_test_mode_info = ("【注意】\n\n"
                                 "1. 您已进入知识库测试模式，您输入的任何对话内容都将用于进行知识库查询，"
                                 "并仅输出知识库匹配出的内容及相似度分值和及输入的文本源路径，查询的内容并不会进入模型查询。\n\n"
                                 "2. 单条内容长度建议设置在100-150左右。\n\n"
                                 )
#####################################
embedding_model_dict_list = list(embedding_model_dict.keys())
llm_model_dict_list = list(llm_model_dict.keys())
from knowledge_service.local_doc_qa import LocalDocQA

local_doc_qa = LocalDocQA()


##########################################
class BaseResponse(BaseModel):
    code: int = pydantic.Field(200, description="HTTP status code")
    msg: str = pydantic.Field("success", description="HTTP status message")

    class Config:
        schema_extra = {
            "example": {
                "code": 200,
                "msg": "success",
            }
        }


"""
上传文件 并写入数据库
:param file_name:  text.txt
:param kb_name: 数据库名称
:param file: 文件的base64编码
:return:
"""


# @app.post('/upload', response_model=BaseResponse)
# def upload_file():
#     data = request.get_json()
#     kb_name = data['kb_name']
#     file_name = data['file_name']
#     file = data['file'].split(',')[-1]
#
#     # 使用uuid作为文件名称 防止文件重复
#     file_name_code = str(uuid.uuid1())
#     # 获取文件后缀名
#     file_extension = file_name.split('.')[-1]
#     # 获取编码后的文件名
#     file_name_new = file_name_code + '.' + file_extension
#
#     resp = None
#     try:
#         # 保存文件至 FILE_SAVE_PATH  一个数据库的文件都写入一个数据库中
#         file_path = os.path.join(FILE_SAVE_PATH, kb_name)
#         if not os.path.exists(file_path):
#             os.makedirs(file_path)
#
#         file_path = os.path.join(file_path, file_name_new)
#         # 写入base64文件
#         with open(file_path, "wb") as f:
#             f.write(base64.b64decode(file))
#         # 创建知识库
#         resp = create_base(file_path, file_name_code)
#         # 写入数据库
#         insert_vector_db_data(kb_name, file_name_code, file_name)
#     except Exception as e:
#         print(str(e))
#     if resp == 'success':
#         return {"status": 200, "message": "success"}
#     else:
#         return {"status": 500, "message": resp}


@app.post('/upload_file', response_model=BaseResponse)
async def upload_file(
        file_name: str = Body(..., description="文件名", example="text.txt"),
        file_base64: str = Body(..., description="filesBase64", example="Base64形式的文件"),
        knowledge_base_id: str = Body(..., description="Knowledge Base Name", example="kb1"),
):
    # 使用uuid作为文件名称 防止文件重复
    file_name_code = str(uuid.uuid1())
    # 获取文件后缀名
    file_extension = file_name.split('.')[-1]
    # 获取编码后的文件名
    file_name_new = file_name_code + '.' + file_extension

    saved_path = os.path.join(UPLOAD_ROOT_PATH, knowledge_base_id)
    if not os.path.exists(saved_path):
        os.makedirs(saved_path)

    file_path_and_name = os.path.join(saved_path, file_name_new)
    # 写入base64文件
    with open(file_path_and_name, "wb") as f:
        f.write(base64.b64decode(file_base64))
    # 知识库 vector 存储路径
    vs_path = os.path.join(VS_ROOT_PATH, knowledge_base_id)

    vs_path, loaded_files = local_doc_qa.init_knowledge_vector_store([file_path_and_name], vs_path)
    if len(loaded_files) > 0:
        file_status = f"文件 {file_name} 已上传至名为{knowledge_base_id}的知识库，并已加载知识库，请开始提问。"
        return BaseResponse(code=200, msg=file_status)
    else:
        file_status = "文件上传失败，请重新上传"
        return BaseResponse(code=500, msg=file_status)


"""
聊天对话(get_answer)
:param query:  用户输入的问题
:param history: 历史数据
:param dbname: 当前查询的数据库
:return:
"""


@app.post('/chat')
async def chat(knowledge_base_id: str = Body(..., description="Knowledge Base Name", example="kb1"),
               question: str = Body(..., description="Question", example="工伤保险是什么？"),
               history: List[List[str]] = Body(
                   [],
                   description="History of previous questions and answers",
                   example=[
                       [
                           "工伤保险是什么？",
                           "工伤保险是指用人单位按照国家规定，为本单位的职工和用人单位的其他人员，缴纳工伤保险费，由保险机构按照国家规定的标准，给予工伤保险待遇的社会保险制度。",
                       ]
                   ],
               ),
               ):
    # 这里直接使用 knowledge_base_id，因为它已经被解析为字符串类型
    vs_path = os.path.join(VS_ROOT_PATH, knowledge_base_id)
    robot_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
    if not os.path.exists(vs_path):
        # return BaseResponse(code=1, msg=f"Knowledge base {knowledge_base_id} not found")
        return ChatMessage(
            question=question,
            response=f"当前知识库{knowledge_base_id}为空，如需基于知识库进行问答，请先加载知识库后，再进行提问",
            history=history,
            robot_time=robot_time,
            source_documents=[],
        )
    else:
        for resp, history in local_doc_qa.get_knowledge_based_answer(
                query=question, vs_path=vs_path, chat_history=history, streaming=False
        ):
            pass
        source_documents = [
            f"""出处 [{inum + 1}] {os.path.split(doc[0].metadata['source'])[-1]}：\n\n{doc[0].page_content}\n\n"""
            for inum, doc in enumerate(resp["source_documents"])
        ]
        return ChatMessage(
            question=question,
            response=resp["result"],
            history=history,
            robot_time=robot_time,
            source_documents=source_documents,
        )


class ChatMessage(BaseModel):
    question: str = pydantic.Field(..., description="Question text")
    response: str = pydantic.Field(..., description="Response text")
    history: List[List[str]] = pydantic.Field(..., description="History text")
    source_documents: List[str] = pydantic.Field(
        ..., description="List of source documents and their scores"
    )
    robot_time: str = pydantic.Field(..., description="Robot time")

    class Config:
        schema_extra = {
            "example": {
                "question": "工伤保险如何办理？",
                "response": "根据已知信息，可以总结如下：\n\n1. 参保单位为员工缴纳工伤保险费，以保障员工在发生工伤时能够获得相应的待遇。\n2. 不同地区的工伤保险缴费规定可能有所不同，需要向当地社保部门咨询以了解具体的缴费标准和规定。\n3. 工伤从业人员及其近亲属需要申请工伤认定，确认享受的待遇资格，并按时缴纳工伤保险费。\n4. 工伤保险待遇包括工伤医疗、康复、辅助器具配置费用、伤残待遇、工亡待遇、一次性工亡补助金等。\n5. 工伤保险待遇领取资格认证包括长期待遇领取人员认证和一次性待遇领取人员认证。\n6. 工伤保险基金支付的待遇项目包括工伤医疗待遇、康复待遇、辅助器具配置费用、一次性工亡补助金、丧葬补助金等。",
                "history": [
                    [
                        "工伤保险是什么？",
                        "工伤保险是指用人单位按照国家规定，为本单位的职工和用人单位的其他人员，缴纳工伤保险费，由保险机构按照国家规定的标准，给予工伤保险待遇的社会保险制度。",
                    ]
                ],
                "source_documents": [
                    "出处 [1] 广州市单位从业的特定人员参加工伤保险办事指引.docx：\n\n\t( 一)  从业单位  (组织)  按“自愿参保”原则，  为未建 立劳动关系的特定从业人员单项参加工伤保险 、缴纳工伤保 险费。",
                    "出处 [2] ...",
                    "出处 [3] ...",
                ],
            }
        }


'''
获取现有知识库
:param query:  none
:param history: none
:param dbname: none
:return:现有知识库
'''


@app.get('/get_vs_list')
def get_vs_list():
    lst_default = ["新建知识库"]
    if not os.path.exists(VS_ROOT_PATH):
        return lst_default
    lst = os.listdir(VS_ROOT_PATH)
    if not lst:
        return lst_default
    lst.sort()
    return lst_default + lst


'''
模型初始化
:param query:  none
:param history: none
:param dbname: none
:return:
'''


@app.get('/init_model', response_model=BaseResponse)
def init_model():
    try:
        local_doc_qa.init_cfg()
        local_doc_qa.llm._call("你好")
        reply = """模型已成功加载，可以开始对话，或从右侧选择模式后开始对话"""
        logger.info(reply)
        return reply
    except Exception as e:
        logger.error(e)
        reply = """模型未成功加载，请到页面左上角"模型配置"选项卡中重新选择后点击"加载模型"按钮"""
        if str(e) == "Unknown platform: darwin":
            logger.info("报错了"
                        " ")
        else:
            logger.info(reply)
        return reply


model_status = init_model()
# model_status = "hi"
llm_model_dict_list = list(llm_model_dict.keys())
embedding_model_dict_list = list(embedding_model_dict.keys())

'''
重新加载模型
:param query:  llm_model, embedding_model, llm_history_len, use_ptuning_v2, use_lora, top_k, history
:param history: none
:param dbname: none
:return:
'''


@app.post('/reinit_model', response_model=BaseResponse)
def reinit_model(llm_model="QwenLLM",
                 embedding_model="moka-ai/m3e-small",
                 llm_history_len=10,
                 #  use_ptuning_v2,
                 #  use_lora,
                 top_k=3,
                 history=[]):
    try:
        print("-----------reinit--------------")
        local_doc_qa.init_cfg(llm_model=llm_model,
                              embedding_model=embedding_model,
                              llm_history_len=llm_history_len,
                              #   use_ptuning_v2=use_ptuning_v2,
                              #   use_lora=use_lora,
                              top_k=top_k, )
        model_status = """模型已成功重新加载，可以开始对话，或从右侧选择模式后开始对话"""
        logger.info(model_status)
    except Exception as e:
        logger.error(e)
        model_status = """模型未成功重新加载，请到页面左上角"模型配置"选项卡中重新选择后点击"加载模型"按钮"""
        logger.info(model_status)
        return {
            "code": 400,
            "msg": "fail"
        }
    return {
        "code": 200,
        "msg": "success"
    }


'''
获取向量数据库
:param query:  llm_model, embedding_model, llm_history_len, use_ptuning_v2, use_lora, top_k, history
:param history: none
:param dbname: none
:return:
'''


@app.post('/get_vector_store', response_model=BaseResponse)
def get_vector_store(vs_id, files, sentence_size, history, one_conent, one_content_segmentation):
    # 构建知识库路径
    vs_path = os.path.join(VS_ROOT_PATH, vs_id)
    # 初始化文件列表
    filelist = []
    # 检查上传路径是否存在，如果不存在则创建
    if not os.path.exists(os.path.join(UPLOAD_ROOT_PATH, vs_id)):
        os.makedirs(os.path.join(UPLOAD_ROOT_PATH, vs_id))
    # 检查语言模型和嵌入模型是否已加载
    if local_doc_qa.llm and local_doc_qa.embeddings:
        # 如果上传的是文件列表
        if isinstance(files, list):
            for file in files:
                # 获取文件名
                filename = os.path.split(file.name)[-1]
                # 移动文件到上传路径
                shutil.move(file.name, os.path.join(UPLOAD_ROOT_PATH, vs_id, filename))
                # 将文件路径添加到文件列表
                filelist.append(os.path.join(UPLOAD_ROOT_PATH, vs_id, filename))
            # 初始化知识库向量存储
            vs_path, loaded_files = local_doc_qa.init_knowledge_vector_store(filelist, vs_path, sentence_size)
        else:
            # 如果上传的是单个文件，调用 one_knowledge_add 方法
            vs_path, loaded_files = local_doc_qa.one_knowledge_add(vs_path, files, one_conent, one_content_segmentation,
                                                                   sentence_size)
        # 检查是否有文件成功加载
        if len(loaded_files):
            # 生成成功状态消息
            file_status = f"已添加 {'、'.join([os.path.split(i)[-1] for i in loaded_files if i])} 内容至知识库，并已加载知识库，请开始提问"
        else:
            # 生成失败状态消息
            file_status = "文件未成功加载，请重新上传文件"
    else:
        # 如果模型未加载，生成错误状态消息
        file_status = "模型未完成加载，请先在加载模型后再导入文件"
        vs_path = None
    # 记录文件状态消息到日志
    logger.info(file_status)
    # 返回知识库路径、文件状态和更新后的历史记录
    return vs_path, None, history + [[None, file_status]]


"""
获取初始化数据
"""


@app.get('/info', response_model=BaseResponse)
def init_info():
    vector_db_list = []
    chat_record_list = []
    hot_list = []
    db_active = '-1'
    resp = {'status': 500, 'dbinfo': None, 'chats': None}
    try:
        # 获取热点数据
        hots = select_hot_problem_data()
        if len(hots) != 0:
            for hot in hots:
                hot_info = {'id': hot[0], 'name': hot[1], 'img': '1.png', 'answer': hot[2]}
                hot_list.append(hot_info)
        # 获取向量数据库信息
        vector_dbs = select_vector_db_data()
        if len(vector_dbs) != 0:
            for db in vector_dbs:
                db_info = {'id': db[0], 'name': db[1], 'alias': db[2], 'img': '2.jpeg'}
                if db[4] == '1':
                    db_active = db[0]
                vector_db_list.append(db_info)
            if db_active == '-1':
                db_active = vector_dbs[0][0]
        # 获取聊天记录
        chat_records = select_chat_record_data(20)
        if len(chat_records) != 0:
            for record in chat_records:
                mine = False
                if record[2] == '1':
                    mine = True
                record_info = {'text': {'text': record[1]}, 'name': record[3], 'mine': record[2] == '1',
                               'date': record[4], 'img': '1.png' if record[2] == '1' else '2.jpeg'}
                chat_record_list.append(record_info)
        db = {'dblist': vector_db_list, 'active': db_active}
        resp = {'status': 200, 'dbinfo': db, 'chats': chat_record_list, 'hot': hot_list}
    except Exception as e:
        print(str(e))
    return resp


if __name__ == '__main__':
    # 初始化数据库
    init_db_service()
    # print(reinit_model(llm_model="chatglm-6b"))
    # exit()
    uvicorn.run(app, host='127.0.0.1', port=3000)
