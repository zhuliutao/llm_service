from configs import *

from langchain_community.vectorstores import FAISS  # or another vector store as per your config
from langchain.memory import ConversationBufferMemory
import os


class doc_qa():

    def __init__(self) -> None:
        logger.info("init...")
        self.init()
        logger.info("init finished!")

    def init(self,
             llm=DEFAULT_MODEL,
             embedding_model=DEFAULT_EMBEDDING,
             llm_history_len=LLM_HISTORY_LEN,
             top_k=VECTOR_SEARCH_TOP_K):

        assert llm in LLM_MODELS.keys(), f"No such LLM: {llm}"
        assert embedding_model in EMBEDDING_MODELS.keys(), f"No such embedding: {embedding_model}"

        self.llm = LLM_MODELS[llm]
        from langchain_community.embeddings.huggingface import HuggingFaceEmbeddings
        logger.info("begin load embedding model")
        self.embedding_model = HuggingFaceEmbeddings(model_name=EMBEDDING_MODELS[embedding_model])
        logger.info(self.embedding_model.model_name)
        self.llm_history_len = llm_history_len
        self.top_k = top_k

        db_list = os.listdir(VS_ROOT_PATH)
        self._db = {}
        for db in db_list:
            db_path = os.path.join(VS_ROOT_PATH, db)
            self._db[db] = db_path

    def add_db(self, db_name="default_db"):
        if db_name in self._db.keys():
            raise ValueError(f"Database with name '{db_name}' already exists.")

        # Initialize a new vector store
        vs_path = os.path.join(VS_ROOT_PATH, f"{db_name}")

        import faiss
        from langchain_community.docstore import InMemoryDocstore
        vectorstore = FAISS(
            embedding_function=self.embedding_model,
            docstore=InMemoryDocstore(),
            index=faiss.IndexFlatL2(len(self.embedding_model.embed_query("get_index"))),
            index_to_docstore_id={}
        )
        vectorstore.save_local(vs_path)
        db_config_file = os.path.join(VS_ROOT_PATH, db_name, db_name + ".json")

        db_config = {"embedding_model_path": self.embedding_model.model_name,
                     "index": len(self.embedding_model.embed_query("get_index"))}
        import json
        with open(db_config_file, 'w', encoding='utf-8') as f:
            json.dump(db_config, f, ensure_ascii=False, indent=4)

        self._db[db_name] = vs_path

    def delete_db(self, db_name):
        logger.info(f"delete db {db_name}")
        if db_name not in self.db_list:
            raise "no this db: {}".format(db_name)
        db_path = self._db[db_name]
        print(self._db)
        import shutil
        shutil.rmtree(db_path)
        return self._db.pop(db_name)

    def load_db(self, db_name="default_db") -> FAISS:
        assert db_name in self._db.keys(), f"no this db {db_name}"

        db_path = self._db[db_name]
        db_config_file = os.path.join(db_path, db_name + ".json")
        import json
        with open(db_config_file, 'r', encoding='utf-8') as f:
            db_config = json.load(f)

        from langchain_community.embeddings.huggingface import HuggingFaceEmbeddings
        logger.info("reload embedding model {}".format(db_config["embedding_model_path"]))
        self.embedding_model = HuggingFaceEmbeddings(model_name=db_config["embedding_model_path"])
        logger.info(self.embedding_model.model_name)

        return FAISS.load_local(self._db[db_name], self.embedding_model, allow_dangerous_deserialization=True)

    @property
    def db_list(self) -> list:
        # rts.remove("新建知识库")
        return list(self._db.keys())

    def load_split_file(self, filepath, chunk_size=CHUNK_SIZE):
        logger.info("begin load and split file")
        from knowledge_service.loader import UnstructuredPaddleImageLoader
        from knowledge_service.loader import UnstructuredPaddlePDFLoader
        from langchain_community.document_loaders import UnstructuredFileLoader
        from knowledge_service.test_spliter import ChineseTextSplitter
        if filepath.lower().endswith(".md"):
            logger.info("load md file: {}".format(filepath))
            loader = UnstructuredFileLoader(filepath, mode="elements")
            docs = loader.load()
        elif filepath.lower().endswith(".pdf"):
            logger.info("load pdf file: {filepath}")
            loader = UnstructuredPaddlePDFLoader(filepath)
            textsplitter = ChineseTextSplitter(pdf=True, sentence_size=chunk_size)
            docs = loader.load_and_split(textsplitter)
        elif filepath.lower().endswith(".jpg") or filepath.lower().endswith(".png"):
            logger.info("load jpg/png file: {filepath}")
            loader = UnstructuredPaddleImageLoader(filepath, mode="elements")
            textsplitter = ChineseTextSplitter(pdf=False, sentence_size=chunk_size)
            docs = loader.load_and_split(text_splitter=textsplitter)
        else:
            logger.info("load file: {}".format(filepath))
            loader = UnstructuredFileLoader(filepath, mode="elements")
            textsplitter = ChineseTextSplitter(pdf=False, sentence_size=chunk_size)
            docs = loader.load_and_split(text_splitter=textsplitter)
        logger.info("load file finished!")
        return docs

    def upload_file(self,
                    file_path: str,
                    db_name: str,
                    chunk_size=CHUNK_SIZE):
        vs_db = self.load_db(db_name)
        assert os.path.exists(file_path), "save file default: {file_path}"

        docs = self.load_split_file(file_path, chunk_size=chunk_size)
        logger.info("add to vs: {}...".format(docs[0]))
        vs_db.add_documents(docs)
        vs_db.save_local(self._db[db_name])
        logger.info("add to vs finished!")

    def local_doc_chat(self,
                       query: str,
                       db_name: str = None,
                       history: list = [],
                       ):
        if history != [[]] and history != []:
            for h in history:
                assert len(h) == 2, "history format illegal: {}".format(h)
        #         if len(h) != 2:
        #             raise ValueError("history format illegal: {}".format(h))   但是为啥每条记录的长度不能是2？
        elif history == [[]]:
            history = []
        docs = []
        if db_name:
            vs_db = self.load_db(db_name)
            docs = vs_db.similarity_search(query, k=self.top_k)
            logger.info("find {} docs".format(len(docs)))

            def generate_prompt(related_docs: list[str], query: str,
                                prompt_template=PROMPT_TEMPLATE) -> str:
                context = "\n".join([doc.page_content for doc in related_docs])
                prompt = prompt_template.replace("{question}", query).replace("{context}", context)
                return prompt

            prompt = generate_prompt(docs, query)
        else:
            prompt = query

        response = self.llm._call(prompt, history=history)
        history += [[query, response]]
        if len(history) > self.llm_history_len:
            logger.info("history stack fulled! pop: {}".format(history.pop(0)))
        return {
            "query": query,
            "response": response,
            "source_documents": docs,
            "history": history
        }

    def get_chatmessage_fewshot(self,
                                question: str):
        from llm_service.chatmessage import GetFewShotChatMessage
        ls = GetFewShotChatMessage()
        final_prompt = ls.get_FSCMPromptTemplate(model_path=KNOWLEDGE_MODEL_PATH, file_path=KNOWLEDGE_FILE_PATH)
        prompt = final_prompt.format_prompt(question=question).to_string()
        # a = self.llm._call(prompt)
        # return a
        # chain = final_prompt | self.llm
        # return chain.invoke({'question': question})
        return prompt


if __name__ == "__main__":
    qa = doc_qa()
    logger.info(str(qa.db_list))
    # qa.add_db(db_name="my_db")
    # qa.upload_file("data\installwindow10.txt", db_name="my_db")
    logger.info(str(qa.db_list))
    print(qa.local_doc_chat(query="你好", history=[["你好", "ni"]], db_name="my_db"))
    qa.delete_db("my_db1")
    # logger.info(str(qa.load_db("my_db1")))
    logger.info(str(qa.db_list))
