import argparse
import os
from typing import List
import time

import pydantic
import uvicorn
from fastapi import Body, FastAPI, File, Form, Query, UploadFile, WebSocket
from fastapi.middleware.cors import CORSMiddleware
from pydantic import BaseModel
from typing_extensions import Annotated
from starlette.responses import RedirectResponse
from doc_qa import doc_qa
from configs import *


class BaseResponse(BaseModel):
    code: int = pydantic.Field(200, description="HTTP status code")
    msg: str = pydantic.Field("success", description="HTTP status message")

    class Config:
        schema_extra = {
            "example": {
                "code": 200,
                "msg": "success",
            }
        }


class ListDocsResponse(BaseResponse):
    data: List[str] = pydantic.Field(..., description="List of document names")

    class Config:
        schema_extra = {
            "example": {
                "code": 200,
                "msg": "success",
                "data": ["doc1.docx", "doc2.pdf", "doc3.txt"],
            }
        }


class StrDocsResponse(BaseResponse):
    data: str = pydantic.Field(..., description="List of document names")

    class Config:
        schema_extra = {
            "example": {
                "code": 200,
                "msg": "success",
                "data": ["doc1.docx", "doc2.pdf", "doc3.txt"],
            }
        }


class ChatMessage(BaseModel):
    question: str = pydantic.Field(..., description="Question text")
    response: str = pydantic.Field(..., description="Response text")
    history: List[List[str]] = pydantic.Field(..., description="History text")
    source_documents: List[str] = pydantic.Field(
        ..., description="List of source documents and their scores"),
    robot_time: str = pydantic.Field(..., description="Robot time")
    code: int = pydantic.Field(200, description="HTTP status code")

    class Config:
        schema_extra = {
            "example": {
                "question": "工伤保险如何办理？",
                "response": "根据已知信息，可以总结如下：\n\n1. 参保单位为员工缴纳工伤保险费，以保障员工在发生工伤时能够获得相应的待遇。\n2. 不同地区的工伤保险缴费规定可能有所不同，需要向当地社保部门咨询以了解具体的缴费标准和规定。\n3. 工伤从业人员及其近亲属需要申请工伤认定，确认享受的待遇资格，并按时缴纳工伤保险费。\n4. 工伤保险待遇包括工伤医疗、康复、辅助器具配置费用、伤残待遇、工亡待遇、一次性工亡补助金等。\n5. 工伤保险待遇领取资格认证包括长期待遇领取人员认证和一次性待遇领取人员认证。\n6. 工伤保险基金支付的待遇项目包括工伤医疗待遇、康复待遇、辅助器具配置费用、一次性工亡补助金、丧葬补助金等。",
                "history": [
                    [
                        "工伤保险是什么？",
                        "工伤保险是指用人单位按照国家规定，为本单位的职工和用人单位的其他人员，缴纳工伤保险费，由保险机构按照国家规定的标准，给予工伤保险待遇的社会保险制度。",
                    ]
                ],
                "source_documents": [
                    "出处 [1] 广州市单位从业的特定人员参加工伤保险办事指引.docx：\n\n\t( 一)  从业单位  (组织)  按“自愿参保”原则，  为未建 立劳动关系的特定从业人员单项参加工伤保险 、缴纳工伤保 险费。",
                    "出处 [2] ...",
                    "出处 [3] ...",
                ],
                "code": 200,
                "robot_time": "2024-05-29 19:00:39"
            }
        }


async def upload_file(
        file: UploadFile = File(description="A single binary file"),
        knowledge_base_id: str = Form(..., description="Knowledge Base Name", examples=["kb1"]),
):
    if knowledge_base_id not in qa.db_list:
        file_status = "该知识库不存在: {}".format(knowledge_base_id)
        return BaseResponse(code=500, msg=file_status)
    file_saved_dir = os.path.join(UPLOAD_ROOT_PATH, knowledge_base_id)
    if not os.path.exists(file_saved_dir):
        os.makedirs(file_saved_dir)

    file_content = await file.read()  # 读取上传文件的内容

    file_path = os.path.join(file_saved_dir, file.filename)
    if os.path.exists(file_path) and os.path.getsize(file_path) == len(file_content):
        file_status = f"文件 {file.filename} 已存在。"
        return BaseResponse(code=200, msg=file_status)

    with open(file_path, "wb") as f:
        f.write(file_content)
    try:
        qa.upload_file(file_path=file_path, db_name=knowledge_base_id, chunk_size=CHUNK_SIZE)
        file_status = f"文件 {file.filename} 已上传至新的知识库，并已加载知识库，请开始提问。"
        return BaseResponse(code=200, msg=file_status)
    except:
        file_status = "文件上传失败，请重新上传"
        return BaseResponse(code=500, msg=file_status)


async def upload_files(
        files: Annotated[
            List[UploadFile], File(description="Multiple files as UploadFile")
        ],
        knowledge_base_id: str = Form(..., description="Knowledge Base Name", examples=["kb1"]),
):
    try:
        for file in files:
            await upload_file(file, knowledge_base_id)
        file_status = f"文件 {[f.filename for f in files]} 已上传至新的知识库，并已加载知识库，请开始提问。"
    except:
        file_status = "文件未成功加载，请重新上传文件"
        return BaseResponse(code=500, msg=file_status)

    return BaseResponse(code=200, msg=file_status)


# OK
async def list_db():
    return ListDocsResponse(data=qa.db_list)


# Ok
async def local_doc_chat(
        knowledge_base_id: str = Body(..., description="Knowledge Base Name", example="kb1"),
        question: str = Body(..., description="Question", example="工伤保险是什么？"),
        history: List[List[str]] = Body(
            [[]],
            description="History of previous questions and answers",
            example=[
                [
                    "工伤保险是什么？",
                    "工伤保险是指用人单位按照国家规定，为本单位的职工和用人单位的其他人员，缴纳工伤保险费，由保险机构按照国家规定的标准，给予工伤保险待遇的社会保险制度。",
                ]
            ],
        ),
):
    robot_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())

    if knowledge_base_id not in qa.db_list:
        return ChatMessage(
            question=question,
            response=f"Knowledge base {knowledge_base_id} not found",
            history=history,
            source_documents=[],
            code=400,
            robot_time=robot_time
        )
    else:
        rst = qa.local_doc_chat(query=question, db_name=knowledge_base_id, history=history)

        source_documents = [
            f"""出处 [{inum + 1}] {os.path.split(doc.metadata['source'])[-1]}：\n\n{doc.page_content}\n\n"""
            for inum, doc in enumerate(rst['source_documents'])
        ]
        return ChatMessage(
            question=rst['query'],
            response=rst['response'],
            history=rst['history'],
            source_documents=source_documents,
            code=200,
            robot_time=robot_time
        )


# OK
async def chat(
        question: str = Body(..., description="Question", example="工伤保险是什么？"),
        history: List[List[str]] = Body(
            [[]],
            description="History of previous questions and answers",
            example=[
                [
                    "工伤保险是什么？",
                    "工伤保险是指用人单位按照国家规定，为本单位的职工和用人单位的其他人员，缴纳工伤保险费，由保险机构按照国家规定的标准，给予工伤保险待遇的社会保险制度。",
                ]
            ],
        ),
):
    robot_time = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime())
    rst = qa.local_doc_chat(query=question, db_name=None, history=history)
    logger.info(rst['response'])

    return ChatMessage(
        question=rst['query'],
        response=rst['response'],
        history=rst['history'],
        code=200,
        source_documents=rst['source_documents'],
        robot_time=robot_time
    )


async def add_db(
        knowledge_base_id: str = Body(...,
                                      embed=True,
                                      description="Knowledge Base Name",
                                      examples=["db1"]),
):
    try:
        qa.add_db(knowledge_base_id)
        return BaseResponse(code=200, msg=f"add {knowledge_base_id} successed")
    except Exception as e:
        print(e)
        return BaseResponse(code=400, msg=f"add {knowledge_base_id} failed")


async def delete_db(
        knowledge_base_id: str
):
    import shutil
    content_path = os.path.join(UPLOAD_ROOT_PATH, knowledge_base_id)
    if os.path.exists(content_path):
        shutil.rmtree(content_path)
    try:
        delete_db_name = qa.delete_db(knowledge_base_id)
        return BaseResponse(code=200, msg=f"delete {delete_db_name} successed")
    except:
        return BaseResponse(code=500, msg=f"db {knowledge_base_id} not found")


async def document():
    return RedirectResponse(url="/docs")


async def init(
        llm_model: str = Body(list(LLM_MODELS.keys())[0],
                              description="llm model name in {}".format(LLM_MODELS.keys()),
                              examples=list(LLM_MODELS.keys())),
        embedding_model: str = Body(list(EMBEDDING_MODELS.keys())[0],
                                    description="embedding model name in {}".format(EMBEDDING_MODELS.keys()),
                                    examples=list(EMBEDDING_MODELS.keys())),
        llm_history_len: int = Body(10, description="llm chat history len", example=10, ),
        top_k: int = Body(3, description="db search top k", example=3)
):
    try:
        qa.init(llm_model, embedding_model, llm_history_len, top_k)
        return BaseResponse(code=200, msg=f"init successed")
    except:
        return BaseResponse(code=400, msg=f"init failed")


async def get_embedding_models():
    data = list(EMBEDDING_MODELS.keys())
    return ListDocsResponse(data=data)


async def get_llm_models():
    data = list(LLM_MODELS.keys())
    return ListDocsResponse(data=data)


async def get_fscm_promp_template(
        question: str = Body(..., embed=True, description="user question", example="不能上网怎么办？")):
    return StrDocsResponse(data=qa.get_chatmessage_fewshot(question))


def api_start(host, port):
    global app
    global qa

    app = FastAPI()
    # Add CORS middleware to allow all origins
    # 在config.py中设置OPEN_DOMAIN=True，允许跨域
    # set OPEN_DOMAIN=True in config.py to allow cross-domain
    # if OPEN_CROSS_DOMAIN:
    #     app.add_middleware(
    #         CORSMiddleware,
    #         allow_origins=["*"],
    #         allow_credentials=True,
    #         allow_methods=["*"],
    #         allow_headers=["*"],
    #     )
    # app.websocket("/local_doc_qa/stream-chat/{knowledge_base_id}")(stream_chat)

    app.get("/", response_model=BaseResponse)(document)

    app.post("/chat", response_model=ChatMessage)(chat)

    app.post("/init", response_model=BaseResponse)(init)
    app.post("/add_db", response_model=BaseResponse)(add_db)
    app.post("/upload_file", response_model=BaseResponse)(upload_file)
    app.post("/upload_files", response_model=BaseResponse)(upload_files)
    app.post("/local_doc_chat", response_model=ChatMessage)(local_doc_chat)
    app.get("/list_db", response_model=ListDocsResponse)(list_db)
    app.get("/get_embedding_models", response_model=ListDocsResponse)(get_embedding_models)
    app.get("/get_llm_models", response_model=ListDocsResponse)(get_llm_models)
    app.delete("/delete_db/{knowledge_base_id}", response_model=BaseResponse)(delete_db)
    app.post("/get_fscm_promp_template", response_model=StrDocsResponse)(get_fscm_promp_template)

    qa = doc_qa()
    uvicorn.run(app, host=host, port=port)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--host", type=str, default="127.0.0.1")
    parser.add_argument("--port", type=int, default=3000)
    args = parser.parse_args()
    api_start(args.host, args.port)
