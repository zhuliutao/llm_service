# init
from llm_service.qwenllm import QwenLLM
from llm_service.chatgml import ChatGLM2

# 匹配后单段上下文长度
CHUNK_SIZE = 250

# LLM input history length
LLM_HISTORY_LEN = 3

# return top-k text chunk from vector store
VECTOR_SEARCH_TOP_K = 5

DEFAULT_MODEL = "qwenllm"
DEFAULT_EMBEDDING = "m3e-small"
LLM_MODELS = {
    "qwenllm": QwenLLM(),
    "chatglm2": ChatGLM2(),
}

EMBEDDING_MODELS = {
    "m3e-small": "../model/embeddings/m3e-small",
    "text2vec-base": "D:/中国电信文件/完成的培训/CBARS2024/A计划中级/实验环境/text2vec-base",
    "bce-embedding": "D:/中国电信文件/完成的培训/CBARS2024/A计划中级/实验环境/bce-embedding-base_v1",
    "rerank": "D:/中国电信文件/完成的培训/CBARS2024/A计划中级/实验环境/rerank"
}

KNOWLEDGE_FILE_PATH = '../data/知识云QA.txt'
KNOWLEDGE_MODEL_PATH = '../model/embeddings/m3e-small'

# file save
import os

UPLOAD_ROOT_PATH = os.path.join(os.path.dirname(os.path.dirname(__file__)), "data/content")

VS_ROOT_PATH = os.path.join(os.path.dirname(os.path.dirname(__file__)), "data/vector_store")

# prompt
PROMPT_TEMPLATE = """已知信息：
{context}

根据上述已知信息，简洁和专业的来回答用户的问题。如果无法从中得到答案，请说 “根据已知信息无法回答该问题” 或 “没有提供足够的相关信息”，不允许在答案中添加编造成分，答案请使用中文。 问题是：{question}"""

# logger

import logging
import sys

LOG_FORMAT = "%(levelname) -5s %(asctime)s" "-1d: %(message)s"
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logging.basicConfig(format=LOG_FORMAT, stream=sys.stdout)
