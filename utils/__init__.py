import torch  # 导入 PyTorch 库

def torch_gc():
    if torch.cuda.is_available():
        # 如果 CUDA 可用
        torch.cuda.empty_cache()  # 释放未使用的显存回到 GPU，以供其他 GPU 进程使用
        torch.cuda.ipc_collect()  # 清理 GPU 上的 IPC 缓存 清理 GPU 上用于 IPC 的显存。这样做的目的是防止由于进程间通信而导致的显存泄漏，确保显存可以被重新分配和有效利用
    elif torch.backends.mps.is_available():
        # 如果 MPS（Metal Performance Shaders，苹果平台上的硬件加速框架）可用
        try:
            from torch.mps import empty_cache  # 从 torch.mps 导入 empty_cache 函数
            empty_cache()  # 清理 MPS 上未使用的内存
        except Exception as e:
            print(e)  # 打印异常信息
            print("如果您使用的是 macOS 建议将 pytorch 版本升级至 2.0.0 或更高版本，以支持及时清理 torch 产生的内存占用。")
            # 提示用户在 macOS 上升级 PyTorch 版本以支持 MPS 内存清理
