import torch.cuda
import torch.backends
import os
import logging
import uuid

FILE_SAVE_PATH = '../data/file'  # 保存文件的位置
DB_FILE_PATH = '../data/db'  # 数据库的保存位置
SQLIT_DATA_PATH = '../data/sqlit/llm_chat.db'


LOG_FORMAT = "%(levelname) -5s %(asctime)s" "-1d: %(message)s"
logger = logging.getLogger()
logger.setLevel(logging.INFO)
logging.basicConfig(format=LOG_FORMAT)

embedding_model_dict = {
    # "text2vec-base": r"model\embeddings\shibing624\text2vec-base-chinese",
    # "bce-embedding": r"model\embeddings\shibing624\text2vec-base-chinese",
    "moka-ai/m3e-small": "../model/embeddings/m3e-small"
}

# Embedding model name
EMBEDDING_MODEL = "moka-ai/m3e-small"

# 本地分词模型路径
EMBEDDING_PATH = '../model/embeddings/m3e-small'

# Embedding running device
# EMBEDDING_DEVICE = "cuda" if torch.cuda.is_available() else "mps" if torch.backends.mps.is_available() else "cpu"
EMBEDDING_DEVICE = "cpu"

# supported LLM models
llm_model_dict = {
    "chatglm-6b": "THUDM/chatglm-6b",
    "QwenLLM": "QwenLLM",
}

# LLM model name
# LLM_MODEL = "chatglm-6b"
LLM_MODEL = "QwenLLM"

# 远程GLM服务路径
LLM_MODEL_URL = "http://101.42.24.69:8000"

# LLM lora path，默认为空，如果有请直接指定文件夹路径
LLM_LORA_PATH = ""
USE_LORA = True if LLM_LORA_PATH else False

# LLM streaming reponse
STREAMING = True

# Use p-tuning-v2 PrefixEncoder
USE_PTUNING_V2 = False

# LLM running device
# LLM_DEVICE = "cuda" if torch.cuda.is_available() else "mps" if torch.backends.mps.is_available() else "cpu"
LLM_DEVICE = "cpu"

# MOSS load in 8bit
LOAD_IN_8BIT = True

# 知识库 vector 存储路径 用于查看当前 vector_store 文件夹下知识库
# VS_ROOT_PATH = os.path.join(os.path.dirname(os.path.dirname(__file__)), "data/vector_store")
VS_ROOT_PATH = "../data/vector_store/"
# VS_ROOT_PATH = r"D:\TotalWorkspace\python\JuypterNotebook\2024A计划\MidDay12Day13Day14RAGProj\database\vector_store"
# VS_ROOT_PATH = r"D:\TEMP\vectorstore"
# 文件上传的 文件夹
UPLOAD_ROOT_PATH = "../content/"
# UPLOAD_ROOT_PATH = os.path.join(os.path.dirname(os.path.dirname(__file__)), "content")


# 基于上下文的prompt模版，请务必保留"{question}"和"{context}"
PROMPT_TEMPLATE = """已知信息：
{context} 

根据上述已知信息，简洁和专业的来回答用户的问题。如果无法从中得到答案，请说 “根据已知信息无法回答该问题” 或 “没有提供足够的相关信息”，不允许在答案中添加编造成分，答案请使用中文。 问题是：{question}"""

# 文本分句长度
SENTENCE_SIZE = 100

# 匹配后单段上下文长度
CHUNK_SIZE = 250

# LLM input history length
LLM_HISTORY_LEN = 3

# return top-k text chunk from vector store
VECTOR_SEARCH_TOP_K = 5

# 知识检索内容相关度 Score, 数值范围约为0-1100，如果为0，则不生效，经测试设置为小于500时，匹配结果更精准
VECTOR_SEARCH_SCORE_THRESHOLD = 0

NLTK_DATA_PATH = os.path.join(os.path.dirname(os.path.dirname(__file__)), "nltk_data")

FLAG_USER_NAME = uuid.uuid4().hex

logger.info(f"""
loading model config
llm device: {LLM_DEVICE}
embedding device: {EMBEDDING_DEVICE}
dir: {os.path.dirname(os.path.dirname(__file__))}
flagging username: {FLAG_USER_NAME}
""")

# 是否开启跨域，默认为False，如果需要开启，请设置为True
# is open cross domain
OPEN_CROSS_DOMAIN = False